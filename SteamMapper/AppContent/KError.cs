﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SteamTurbine
{

    public class KError
    {
        public enum Code
        {
            S_OK,
            InvalidCredentials,
            Error,
            SettingsNotFound,
            RegistryError
        }
    }
}
