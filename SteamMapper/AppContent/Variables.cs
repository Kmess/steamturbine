﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SteamMapper;
using System.IO;
using System.Windows;
using Microsoft.Win32;
using Google.GData.Client;
using Google.GData.Extensions;
using Google.GData.Spreadsheets;
using System.Xml.Serialization;
using System.Security.Cryptography;

namespace SteamTurbine
{
    class Variables
    {
        public static double groupBoxAccountHeight_Init = 0;
        public static double defaultGridHeight = 150;
        public static double defaultGridWidth = 330;
        public static Thickness defaultRightMargins = new Thickness(385, 10, -165, 0);
        public static AnimationSettings defaultCenterGrids = new AnimationSettings(MainWindow.AnimationDef.Current, MainWindow.AnimationDef.Right, MainWindow.AnimationDef.Left);
        public static AnimationSettings defaultRightGrids = new AnimationSettings(MainWindow.AnimationDef.Right, MainWindow.AnimationDef.Current, MainWindow.AnimationDef.Left);

    }

    class AnimationSettings
    {
        public SteamTurbine.MainWindow.AnimationDef currentLocation;
        public SteamTurbine.MainWindow.AnimationDef previousAction;
        public SteamTurbine.MainWindow.AnimationDef nextAction;

        public AnimationSettings(MainWindow.AnimationDef current, MainWindow.AnimationDef previous, MainWindow.AnimationDef next)
        {
            currentLocation = current;
            previousAction = previous;
            nextAction = next;
        }
    }

    class XMLParse
    {
        string path;
        public XMLParse()
        {
            path = AppDomain.CurrentDomain.BaseDirectory + @"AppSettings.xml";
        }

        public Settings_SteamTurbine LoadXMLSettings()
        {
            try
            {            
                if (File.Exists(path))
                {
                    System.Xml.Serialization.XmlSerializer read = new System.Xml.Serialization.XmlSerializer(typeof(Settings_SteamTurbine));
                    System.IO.StreamReader file = new StreamReader(path);

                    Settings_SteamTurbine savedSettings = new Settings_SteamTurbine();
                    savedSettings = (Settings_SteamTurbine)read.Deserialize(file);
                    file.Close();

                    return savedSettings;
                }
                else
                {
                    return null;
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("The process failed: {0}", e.ToString());
                return null;
            }
        }

        public void SaveXMLSettings(Settings_SteamTurbine activeSettings)
        {
            try
            {
                System.Xml.Serialization.XmlSerializer writer = new System.Xml.Serialization.XmlSerializer(typeof(Settings_SteamTurbine));
                System.IO.StreamWriter file = new System.IO.StreamWriter(path);
                writer.Serialize(file, activeSettings);
                file.Close();
            } 
            catch (Exception e)
            {
                Console.WriteLine("The process failed: {0}", e.ToString());
            }
        }
    }

    public class Settings_SteamTurbine
    {
        public string GoogleSpreadsheetURI;
        public string GoogleUserNameEnc;
        public string GooglePasswordEnc;
        public string SteamLocation;
        public bool ExecuteOnStart;
        public string CSVLocation;
        public string OneAccountUserNameEnc;
        public string OneAccountPasswordEnc;
        public string ActiveMode;
        public bool MinimizeToTray;
        public bool interceptSteamAggresive;
        public bool deleteClientRegistry;
        public bool WindowsIsHidden;

        public Settings_SteamTurbine()
        {
            
        }             

        public void DecryptCredentials()
        {
            if (ActiveMode == "GoogleDocs")
                GooglePasswordEnc = DecryptPassword(GooglePasswordEnc);
        }

        public void EncryptCredentials(bool status)
        {
            if(status)
            {
                if (ActiveMode == "GoogleDocs")
                    GooglePasswordEnc = EncryptPassword(GooglePasswordEnc);
            } else
            {
                if (ActiveMode == "GoogleDocs")
                    GooglePasswordEnc = DecryptPassword(GooglePasswordEnc);
            }
        }
        public string EncryptPassword(string txtPassword)
        {
            byte[] passBytes = System.Text.Encoding.Unicode.GetBytes(txtPassword);
            //passBytes[0]++;
            //passBytes[5]++;
            //passBytes[10]++;
            //string encryptPassword = Convert.ToBase64String(passBytes);

            // Create some random entropy. 
            //byte[] entropy = CreateRandomEntropy();

            //byte[] encrypted = ProtectedData.Protect(passBytes, null, DataProtectionScope.LocalMachine);
            string encryptPassword = Convert.ToBase64String(passBytes);
            return encryptPassword;
        }

        public static byte[] CreateRandomEntropy()
        {
            // Create a byte array to hold the random value. 
            byte[] entropy = new byte[16];

            // Create a new instance of the RNGCryptoServiceProvider. 
            // Fill the array with a random value. 
            new RNGCryptoServiceProvider().GetBytes(entropy);

            // Return the array. 
            return entropy;
        }

        public string DecryptPassword(string encryptedPassword)
        {
            try
            {
                byte[] passByteData = Convert.FromBase64String(encryptedPassword);
                //passByteData[0]--;
                //passByteData[5]--;
                //passByteData[10]--;

                //byte[] decrypt = ProtectedData.Unprotect(passByteData, null, DataProtectionScope.LocalMachine);
                string originalPassword = System.Text.Encoding.Unicode.GetString(passByteData);
                return originalPassword;
            }
            catch (Exception e)
            {
                Console.WriteLine("Error - faulty password");
            }

            return null;
        }

        public string getSteamExe(out KError.Code code)
        {
            code = KError.Code.S_OK;
            string steamCurrentUserInstallAddress = "SOFTWARE\\Valve\\Steam";
            string steamPath = "SteamPath";

            try
            { 
            RegistryKey steam = Registry.CurrentUser.OpenSubKey
                 (steamCurrentUserInstallAddress);

            if (steam.GetValue(steamPath) != null)
            {
                SteamLocation = steam.GetValue(steamPath).ToString();
                if (SteamLocation.Contains('/'))
                    SteamLocation = SteamLocation.Replace("/", @"\");
                return SteamLocation;
            }
            } 
            catch (Exception e)
            {
                Console.WriteLine("Error with {0}", e);
                code = KError.Code.RegistryError;
                return null;
            }
            return null;
        }

        public bool isSteamIntercept()
        {
            return interceptSteamAggresive;
        }

        public void setSteamIntercept(bool status)
        {
            interceptSteamAggresive = status;
        }

        public void setHiddenMode(bool status)
        {
            WindowsIsHidden = status;
        }

        public string getActiveMode()
        {
            return ActiveMode;
        }
        public bool getStartup()
        {
            string CurrentUserStartupAddress = "SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Run";
            string AppName = "Turbine";


            RegistryKey startup = Registry.CurrentUser.OpenSubKey
                (CurrentUserStartupAddress, true);

            if (startup.GetValue(AppName) != null)
            {
                return true;
            }
            return false;
        }

        public void clear()
        {
            GoogleSpreadsheetURI = null;
            GoogleUserNameEnc = null;
            GooglePasswordEnc = null;
            SteamLocation = null;
            ExecuteOnStart = false;
            CSVLocation = null;
            OneAccountUserNameEnc = null;
            OneAccountPasswordEnc = null;
            ActiveMode = null;
            MinimizeToTray = false;
            interceptSteamAggresive = false;
        }


        public void setSteamPath(string s)
        {
            SteamLocation = s;
        }
        public void setStartup(bool option)
        {
            string CurrentUserStartupAddress = "SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Run";
            string AppName = "Turbine";

            RegistryKey startup = Registry.CurrentUser.OpenSubKey(CurrentUserStartupAddress, true);

            if (option)
            {
                startup.SetValue(AppName, System.Reflection.Assembly.GetEntryAssembly().Location);
            }
            else
            {
                startup.DeleteValue(AppName, false);
            }
        }

        public void setOneAccountCred(string username, string password)
        {
            OneAccountUserNameEnc = username;
            OneAccountPasswordEnc = password;
        }

        public void setGoogleCred(string username, string password)
        {
            GoogleUserNameEnc = username;
            GooglePasswordEnc = password;
        }

        public void setGoogleURI(SpreadsheetEntry mySSID)
        {
            GoogleSpreadsheetURI = mySSID.SelfUri.ToString();
        }
        public void setActiveMode(string mode)
        {
            ActiveMode = mode;
        }

        public void setExecuteOnStart (bool status)
        {
            ExecuteOnStart = status;
        }
        public void save()
        {
            System.Xml.Serialization.XmlSerializer writer = new System.Xml.Serialization.XmlSerializer(typeof(Settings_SteamTurbine));
            System.IO.StreamWriter file = new System.IO.StreamWriter(@"AppSettings.xml");
            writer.Serialize(file, this);
            file.Close();
        }
    }

}
