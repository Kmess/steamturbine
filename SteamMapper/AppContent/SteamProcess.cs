﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;
using System.Windows;

namespace SteamTurbine
{
    class SteamProcess
    {
        private static bool statusIntercept;
        private int pid;
        private Process Steam;
        private string username, password;

        public SteamProcess(string user, string pass, string location)
        {
            Steam = new Process();
            Steam.StartInfo.FileName = location;
            username = user;
            password = pass;
        }

        public void setSteamCred (string[] cred)
        {
            username = cred[0];
            password = cred[1];
        }
        public bool setIntercept(bool status)
        {
            statusIntercept = status;
            return true;
        }

        public async Task<int> SteamIntercept(bool intercepts)
        {
            Process[] localbyname;
            localbyname = Process.GetProcessesByName("Steam");

            if (!intercepts)
            {
                try
                {
                    if (localbyname[0].Id != pid)
                    {
                        SteamExit(localbyname[0]);
                        SteamLaunch();
                    }
                }
                catch (IndexOutOfRangeException e)
                {
                    Console.WriteLine("Steam currently not running");
                    SteamLaunch();
                }
            }
            else
            {



                if (statusIntercept == true)
                    setIntercept(false);
                else
                {
                    setIntercept(true);
                }



                while (statusIntercept)
                {
                    await Task.Delay(3000);
                    localbyname = Process.GetProcessesByName("Steam");
                    try
                    {
                        if (localbyname[0].Id != pid)
                        {
                            SteamExit(localbyname[0]);
                            SteamLaunch();

                        }
                        else
                            Console.WriteLine("Managing...");
                    }
                    catch (IndexOutOfRangeException e)
                    {
                        Console.WriteLine("Steam currently not running");
                        SteamLaunch();
                    }
                }
            }
            return 1;

            
        }

        private void SteamLaunch()
        {
            Steam.StartInfo.Arguments = "-login "+username+ " "+ password;
            Steam.Start();
            pid = Steam.Id;
        }

        private void SteamExit(Process dSteam)
        {
            if (!dSteam.HasExited)
            {
                dSteam.Kill();
            }
        }
    }
}
