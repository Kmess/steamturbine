﻿using Google.GData.Client;
using Google.GData.Extensions;
using Google.GData.Spreadsheets;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;



namespace SteamTurbine
{
    public class GClientDrive
    {

        private string userName { get; set; }
        private string passWord { get; set; }
        private string publicURL { get; set; }
        private bool saveCred { get; set; }
        private string URI { get; set; }
        private SpreadsheetsService myService { get; set; }
        private MainWindow mainWindow { get; set; }
        public SpreadsheetEntry targetSpreadsheet { get; private set; }
        private bool exceptionOccured;

        
        public GClientDrive(string username, string password, MainWindow main, bool save)
        {
            mainWindow = main;
            userName = username;
            passWord = password;
            URI = "";
            saveCred = save;
        }

        public GClientDrive(string username, string password, string stringURI)
        {
            userName = username;
            passWord = password;
            URI = stringURI;
        }

        public bool checkException()
        {
            if (exceptionOccured)
            {
                exceptionOccured = false;
                return true;
            }
            else
                return false;
        }
        public string[] GDriveGetSteamCredentials()
        {
            MainWindow.ErrorCodes result;
            string[] steamcred = new string[2];
            steamcred = GDriveAuth(out result);
            while (result != MainWindow.ErrorCodes.S_OK)
            {
                steamcred = GDriveAuth(out result);
                Console.WriteLine("Waiting for 1 second");
                Task.Delay(1000);
            }
                return steamcred;
        }
        public string[] GDriveAuth(out MainWindow.ErrorCodes result)
        {
            result = MainWindow.ErrorCodes.S_OK;

            string[] steamCred = new string[2];

            myService = new SpreadsheetsService("AutoSteam");
            myService.setUserCredentials(userName, passWord);

            try
            {
                if (URI == "")
                {

                    mainWindow.Dispatcher.Invoke((Action)(() =>
                    {
                        mainWindow.TextBlockStatusGoogle.Text = "Polling for Documents...";

                    }));
                    SpreadsheetQuery query = new SpreadsheetQuery();


                    SpreadsheetFeed feed = myService.Query(query);

                    List<string> listofsheets = new List<string>();
                    List<SpreadsheetEntry> listofentries = new List<SpreadsheetEntry>();
                    int i = 0;

                    foreach (SpreadsheetEntry entry in feed.Entries)
                    {
                        listofsheets.Add(entry.Title.Text);
                        listofentries.Add(entry);
                        i++;
                    }

                    mainWindow.Dispatcher.Invoke((Action)(() =>
                    {
                        List<ListViewItem> lvitems = new List<ListViewItem>();

                        foreach (var s in listofsheets.Zip(listofentries, (a, b) => new { A = a, B = b }))
                        {
                            ListViewItem lvitem = new ListViewItem();
                            lvitem.Content = s.A;
                            lvitem.Tag = s.B;
                            lvitems.Add(lvitem);
                        }

                        mainWindow.progressBarGoogle.Visibility = System.Windows.Visibility.Hidden;
                        mainWindow.TextBlockStatusGoogle.Text = "";
                        mainWindow.listviewGoogleSheets.ItemsSource = lvitems;
                    }));
                }
                else
                {
                    uint targetRow = 0, targetColumnUser = 0;
                    string username = "", password = "";

                    AtomEntry newEntry = new AtomEntry();
                    FeedQuery singleQuery = new FeedQuery();
                    singleQuery.Uri = new Uri(URI);

                    AtomFeed newFeed = myService.Query(singleQuery);
                    AtomEntry retreivedEntry = newFeed.Entries[0];

                    AtomLink link = retreivedEntry.Links[0];

                    WorksheetQuery wQuery = new WorksheetQuery(link.HRef.ToString());
                    WorksheetFeed wFeed = myService.Query(wQuery);

                    //retrieve the cells in a worksheet
                    WorksheetEntry worksheetEntry = (WorksheetEntry)wFeed.Entries[0];
                    AtomLink cLink = worksheetEntry.Links.FindService(GDataSpreadsheetsNameTable.CellRel, null);

                    //Searching down the row for the Computer name
                    CellQuery cQuery = new CellQuery(cLink.HRef.ToString());
                    CellFeed cFeed = myService.Query(cQuery);

                    foreach (CellEntry cCell in cFeed.Entries)
                    {
                        if (password != "")
                        {
                            if (cCell.Cell.Value == "Active")
                            {
                                //Console.WriteLine(password);
                                break;
                            }
                            else
                            {
                                username = "";
                                password = "";
                                break;
                            }
                        }
                        if (username != "")
                        {
                            password = cCell.Cell.Value;
                        }
                        else
                        {
                            if (targetColumnUser != 0)
                            {
                                username = cCell.Cell.Value;
                                steamCred[0] = username;
                            }
                        }
                        if (cCell.Cell.Value == System.Environment.MachineName)
                        {
                            //Found computer name, recording username and password locations
                            targetRow = cCell.Cell.Row;
                            targetColumnUser = cCell.Cell.Column + 1;
                        }

                    }

                    steamCred[1] = password;

                    
                    return steamCred;
                }

            }
            catch (Exception e)
            {
                if (e.Message == "Invalid credentials")
                    result = MainWindow.ErrorCodes.InvalidCredentials;
                else
                    result = MainWindow.ErrorCodes.Error;
                //System.Windows.Forms.MessageBox.Show(e.Message);
                Console.WriteLine(e.Message);
              //  Task.Delay(1000);
                Console.WriteLine("Waiting for 1 second, no internet detected");
              //  GDriveAuth();
            }
            return null;
        }

    }
}
