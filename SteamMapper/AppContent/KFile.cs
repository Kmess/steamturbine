﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace SteamTurbine.AppContent
{
    public static class KFile
    {
       public static void DeleteFile(string file)
       {
           if (File.Exists(file))
           {
               File.Delete(file);
           }

       }
    }
}
