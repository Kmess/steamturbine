﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using Google.GData.Client;
using Google.GData.Extensions;
using Google.GData.Spreadsheets;

namespace SteamTurbine
{
    class TManager
    {
        Settings_SteamTurbine appSettings;
        SteamProcess steamProcess;
        private GClientDrive GDrive;
        public TManager()
        {
            
        }
        public async void InitParse(SteamTurbine.MainWindow mWindow)
        {
            KError.Code kCode = KError.Code.S_OK;

            if (tryLoadData() == KError.Code.SettingsNotFound)
            {
                //Unable to find settings file, so lets reset the GUI to defaults
                kCode = updateGUILayout(true, mWindow);
                SeedGenerator test = new SeedGenerator();
            } else
            {
                //Lets make sure Steam exists before we continue
                if (!File.Exists(appSettings.SteamLocation))
                {
                    appSettings.SteamLocation = null;
                } else
                {
                    steamProcess = new SteamProcess(appSettings.OneAccountUserNameEnc, appSettings.OneAccountPasswordEnc, appSettings.SteamLocation);
                }
            }

            kCode = updateGUIControls(mWindow);
            updateGUILayout(true, mWindow);

            if (appSettings.ExecuteOnStart) await interceptSteam(appSettings.isSteamIntercept());
        }

        public bool StartupStatus()
        {
            return appSettings.ExecuteOnStart;
        }

        private async Task<KError.Code> interceptSteam (bool interceptAction)
        {
            if (!appSettings.getStartup()) appSettings.setStartup(true); 
            
            switch (appSettings.getActiveMode())
            {
                case "OneAccount":
                    await steamProcess.SteamIntercept(appSettings.isSteamIntercept());
                    break;
                case "GoogleDocs":
                    GoogleDocsExec();
                    break;
                case "CSV":
                    break;
                default:
                    break;
            }
            return KError.Code.S_OK;
        }

        private async void GoogleDocsExec()
        {
            GDrive = new GClientDrive(appSettings.GoogleUserNameEnc, appSettings.GooglePasswordEnc, appSettings.GoogleSpreadsheetURI);
            string[] steamCred = new string[2];
            steamCred = GDrive.GDriveGetSteamCredentials();
            if (steamCred != null)
            {
                steamProcess.setSteamCred(steamCred);
                await steamProcess.SteamIntercept(appSettings.isSteamIntercept());
            }
        }

        private KError.Code updateGUILayout (bool isDefaultLayout, SteamTurbine.MainWindow mWindow)
        {
            if (appSettings.WindowsIsHidden) mWindow.Visibility = System.Windows.Visibility.Hidden;
            return KError.Code.S_OK;
        }

        private KError.Code updateGUIControls(SteamTurbine.MainWindow mWindow)
        {
            KError.Code kCode = KError.Code.S_OK;

            //Updates GUI controls on MainWindow 
            if (appSettings.SteamLocation == null)
            {
                mWindow.TextBoxSteamPath.Text = appSettings.getSteamExe(out kCode) + @"\steam.exe";
            } else
            {
                mWindow.TextBoxSteamPath.Text = appSettings.SteamLocation;
            }

            mWindow.activeModetext.Text = appSettings.ActiveMode ?? "None";
            mWindow.TextBoxGooglePassword.Password = appSettings.GooglePasswordEnc ?? "";
            mWindow.TextBoxGoogleUsername.Text = appSettings.GoogleUserNameEnc ?? "";
            mWindow.executeOnStart.IsChecked = appSettings.ExecuteOnStart;
            mWindow.checkboxInterceptSteam.IsChecked = appSettings.interceptSteamAggresive;

            return kCode;
        }

        private KError.Code tryLoadData()
        {
            //Loads previous set settings if found
            XMLParse xmlSettings = new XMLParse();
            appSettings = xmlSettings.LoadXMLSettings();

            if (appSettings == null)
            {
                //Settings file not found. Create blank instance and return
                appSettings = new Settings_SteamTurbine();
                return KError.Code.SettingsNotFound;
            }

            //At this point we have valid content so lets decrypt the passwords
            appSettings.DecryptCredentials();

            return KError.Code.S_OK;
        }
    }
}
