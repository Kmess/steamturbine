﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using SteamTurbine.AppContent;
using System.Diagnostics;
using System.Windows.Media.Animation;
using Google.GData.Client;
using Google.GData.Extensions;
using Google.GData.Spreadsheets;
using System.IO;
using Microsoft.Win32;
using System.Security.Cryptography;

namespace SteamTurbine
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        Settings_SteamTurbine appSettings;
        SteamProcess steam_process;
        private GClientDrive GDrive;

        //Animation Enums - need to see what we can delete
        public enum AnimationDef
        {
            Enter,
            Bye,
            Right,
            Left,
            Current,
            Advance,
            Previous
        }

        public enum ErrorCodes
        {
            S_OK,
            InvalidCredentials,
            Error
        }

        public enum AccessMode
        {
            GoogleDocs,
            OneAccount,
            CSV
        }

        public MainWindow()
        {
            InitializeComponent();            
        }

        private async void GoogleDocsExec()
        {
            GDrive = new GClientDrive(appSettings.GoogleUserNameEnc, appSettings.GooglePasswordEnc, appSettings.GoogleSpreadsheetURI);
            string[] steamCred = new string[2];
            steamCred = GDrive.GDriveGetSteamCredentials();
            if (steamCred != null)
            {
                steam_process.setSteamCred(steamCred);
                await steam_process.SteamIntercept(appSettings.isSteamIntercept());
            } 
        }
        private async void Window_Initialized(object sender, EventArgs e)
        {
            KError.Code temp;

            //When Window is Initialized, we want to have the TManager parse through this Window
            TManager tManager = new TManager();
            tManager.InitParse(this);

            //Resets the layout to being default
            startOverGrids();
            /*
            //Loads previous set settings if found
            var xmlSettings = new XMLParse();
            appSettings = xmlSettings.LoadXMLSettings() ?? new Settings_SteamTurbine();
            appSettings.EncryptCredentials(false); //Decrypts passwords

            //TODO: We need to see if the location Steam.exe exists. If not, then pull the registry!
            if (!File.Exists(appSettings.SteamLocation))
                appSettings.SteamLocation = null;

            //Updates the GUI controls
            TextBoxSteamPath.Text = appSettings.SteamLocation ?? appSettings.getSteamExe(out temp) + @"\steam.exe";
            activeModetext.Text = appSettings.ActiveMode ?? "None";
            TextBoxGooglePassword.Password = appSettings.GooglePasswordEnc ?? "";
            TextBoxGoogleUsername.Text = appSettings.GoogleUserNameEnc ?? "";
            

            //Sets reference for launching Steam 
            steam_process = new SteamProcess(appSettings.OneAccountUserNameEnc, appSettings.OneAccountPasswordEnc, appSettings.SteamLocation);
            
            if (appSettings.ActiveMode != "" && appSettings.ActiveMode != null)
            {
                ReSizeGroup(GroupBoxAccount, GroupBoxAccount.Height, 200);
                CompletedGrid.Margin = new Thickness(0, 10, -215, 0);

                FadeGrid(gridOneAccount, 1.0, 0);
                FadeCombo(ComboBoxIntegration, 1.0, 0);
                FadeText(TextBlockIntegration, 1.0, 0);
                FadeGrid(CompletedGrid, 0, 1.0);

                activeModetext.Text = appSettings.ActiveMode;
                
 
            }

            if (appSettings.ExecuteOnStart)
            {
                //Sets the registry key just in case app is starting w/ a valid config file
                if (!appSettings.getStartup()) { appSettings.setStartup(true); }   

                //Sets Window Visibility
                if (appSettings.WindowsIsHidden) {this.Visibility = System.Windows.Visibility.Hidden;}            

                executeOnStart.IsChecked = true;
                checkboxInterceptSteam.IsChecked = (bool)appSettings.isSteamIntercept();

                switch (appSettings.getActiveMode())
                {
                    case "OneAccount":
                        await steam_process.SteamIntercept(appSettings.isSteamIntercept());
                        break;
                    case "GoogleDocs":
                        GoogleDocsExec();
                        break;
                    case "CSV":
                        break;
                    default:
                        break;
                }


                //We have to hide to task bar or NOT
            }          
            checkbox_startup.IsChecked = appSettings.getStartup();
             * */

        }

        public void startOverGrids()
        {

            enumerateGoogleSettings(false);
            ReSizeGroup(GroupBoxAccount, GroupBoxAccount.Height, GridAccounts.Height + 15);
            gridGoogle.Margin = new Thickness(0, 10, 0, 0);

            FadeCombo(ComboBoxIntegration, 0, 1.0);
            FadeText(TextBlockIntegration, 0, 1.0);

            FadeGrid(CompletedGrid, 1.0, 0.0);
            FadeGrid(gridListViewGoogle, 1.0, 0.0);

            ComboBoxIntegration.SelectedIndex = 1;

            //Set the grids animation tags
            gridListViewGoogle.Tag = Variables.defaultRightGrids;
            CompletedGrid.Tag = Variables.defaultRightGrids;
            gridOneAccount.Tag = Variables.defaultCenterGrids;
            gridListViewGoogle.Margin = new Thickness(0, 10, 0, 0);
            CompletedGrid.Margin = new Thickness(0, 10, 0, 0);
            gridOneAccount.Margin = new Thickness(0, 10, 0, 0);
            gridListViewGoogle.Margin = new Thickness(0, 10, 0, 0);

            //Need to change based on settings
            GridAccounts.Height = 150;         

            //Controls Init
            progressBarGoogle.Visibility = Visibility.Hidden;
        }

        private void checkboxInterceptSteam_Checked(object sender, RoutedEventArgs e)
        {
            //await steam_process.SteamIntercept();
        }

        private void checkboxInterceptSteam_Unchecked(object sender, RoutedEventArgs e)
        {
            steam_process.setIntercept(false);
            SteamTurbine.Properties.Settings test = new Properties.Settings();
            Console.WriteLine(test.InterceptSteam);
        }

        private void enumerateCSVSettings(bool status)
        {
            if (status)
            {
                enumerateGoogleSettings(false);
                enumerateOneAccountSettings(false);

                //CSV Account Controls Enabled
                gridCSV.IsEnabled = true;
                FadeGrid(gridCSV, 0.0, 1.0);
                gridCSV.Margin = new Thickness(0, 10, -215, 0);
                GridAccounts.Height = 100;
                ReSizeGroup(GroupBoxAccount, GroupBoxAccount.Height, GridAccounts.Height + 15);
            }
            else
            {
                if (gridCSV.IsEnabled != false)
                {
                    FadeGrid(gridCSV, 1.0, 0.0);
                    gridCSV.IsEnabled = false;
                }
            }
        }

        private void enumerateOneAccountSettings(bool status)
        {
            if (status)
            {
                enumerateGoogleSettings(false);
                enumerateCSVSettings(false);




                //One Account Enablement
                gridOneAccount.IsEnabled = true;
                FadeGrid(gridOneAccount, 0.0, 1.0);
                //gridOneAccount.Margin = new Thickness(0, 10, -215, 0);
                GridAccounts.Height = 150;
                //GroupBoxAccount.Height = GridAccounts.Height + 50;

                ReSizeGroup(GroupBoxAccount, GroupBoxAccount.Height, GridAccounts.Height + 15);
            }
            else
            {
                if (gridOneAccount.IsEnabled != false)
                {
                    FadeGrid(gridOneAccount, 1.0, 0.0);
                    gridOneAccount.IsEnabled = false;
                }
            }

        }

        private void enumerateGoogleSettings(bool status)
        {
            if (status)
            {
                enumerateOneAccountSettings(false);
               enumerateCSVSettings(false);




                //Google Account Enable
                gridGoogle.IsEnabled = true;


                gridGoogle.Visibility = System.Windows.Visibility.Visible;
                FadeGrid(gridGoogle, 0.0, 1.0);
                //gridGoogle.Margin = new Thickness(0, 10, -215, 0);
                //GroupBoxAccount.Height = 221;
                GridAccounts.Height = 240;
                ReSizeGroup(GroupBoxAccount, GroupBoxAccount.Height, 200);

                TextBlockStatusGoogle.Text = "";
            }
            else
            {
                if (gridGoogle.IsEnabled != false)
                {
                    //Google Account Disable
                    gridGoogle.IsEnabled = false;
                    FadeGrid(gridGoogle, 1.0, 0.0);
                }
            }         

            
        }

        private void ButtonApply_Click(object sender, RoutedEventArgs e)
        {
            appSettings.setStartup((bool)checkbox_startup.IsChecked);
            appSettings.setSteamPath(TextBoxSteamPath.Text);
            appSettings.setExecuteOnStart((bool)executeOnStart.IsChecked);
            appSettings.setSteamIntercept((bool)checkboxInterceptSteam.IsChecked);
            appSettings.setHiddenMode((bool)checkboxHidden.IsChecked);
            appSettings.EncryptCredentials(true);

            var xmlSettings = new XMLParse();
            xmlSettings.SaveXMLSettings(appSettings);
        }

        private void ComboBoxIntegration_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (this.IsInitialized)
            {
                ComboBoxItem testCombo = new ComboBoxItem();
                testCombo = ComboBoxIntegration.SelectedItem as ComboBoxItem;

                if (testCombo.Content as string == "Google Drive")
                    enumerateGoogleSettings(true);
                else if (testCombo.Content as string == "One Account")
                    enumerateOneAccountSettings(true);
                else
                    enumerateCSVSettings(true);
            }
        }

        private async void ButtonLogin_Click(object sender, RoutedEventArgs e)
        {
            ErrorCodes result;
            progressBarGoogle.Visibility = Visibility.Visible;
            gridGoogle.IsEnabled = false;
            TextBlockStatusGoogle.Text = "Authenticating...";
            bool status = (bool)checkboxSaveCredGoogle.IsChecked; //Not sure if we need this

            GDrive = new GClientDrive(TextBoxGoogleUsername.Text, TextBoxGooglePassword.Password, this, status);

            ReSizeGroup(GroupBoxAccount, GroupBoxAccount.Height, 240);

            await Task.Factory.StartNew<string[]>(() =>GDrive.GDriveAuth(out result));



                await Task.Delay(500);
                hideComboText();
                FadeGrid(gridGoogle, 1.0, 0);
                FadeGrid(gridListViewGoogle, 0, 1.0);


            gridGoogle.IsEnabled = true;
        }

        private void hideComboText()
        {
            FadeCombo(ComboBoxIntegration, 1.0, 0.0);
            FadeText(TextBlockIntegration, 1.0, 0.0);
        }


        private void FadeGrid(Grid gridToAnimate, double from, double to)
        {
            if (from == 1)
            {
                gridToAnimate.Margin = new Thickness(0, 10, 0, 0);
                gridToAnimate.Visibility = System.Windows.Visibility.Visible;
            }
            else
                gridToAnimate.Visibility = System.Windows.Visibility.Hidden;

                DoubleAnimation myDoubleAnimation = new DoubleAnimation();
                myDoubleAnimation.From = from;
                myDoubleAnimation.To = to;
                myDoubleAnimation.Duration = new Duration(TimeSpan.FromSeconds(.2));
                myDoubleAnimation.AutoReverse = false;

                Storyboard myStoryBoard = new Storyboard();
                myStoryBoard.Children.Add(myDoubleAnimation);
                Storyboard.SetTargetName(myDoubleAnimation, gridToAnimate.Name);
                Storyboard.SetTargetProperty(myDoubleAnimation, new PropertyPath(Grid.OpacityProperty));
                myStoryBoard.Begin(this);

                if (to == 0)
                {
                    gridToAnimate.Visibility = Visibility.Hidden;
                    //gridToAnimate.Margin = new Thickness(0, 1000, 0, 0);
                }
                else
                    gridToAnimate.Visibility = Visibility.Visible;

        }

        private void FadeCombo(ComboBox gridToAnimate, double from, double to)
        {
            if (from == 1)
                gridToAnimate.Visibility = System.Windows.Visibility.Visible;
            else
                gridToAnimate.Visibility = System.Windows.Visibility.Hidden;

            DoubleAnimation myDoubleAnimation = new DoubleAnimation();
            myDoubleAnimation.From = from;
            myDoubleAnimation.To = to;
            myDoubleAnimation.Duration = new Duration(TimeSpan.FromSeconds(.2));
            myDoubleAnimation.AutoReverse = false;

            Storyboard myStoryBoard = new Storyboard();
            myStoryBoard.Children.Add(myDoubleAnimation);
            Storyboard.SetTargetName(myDoubleAnimation, gridToAnimate.Name);
            Storyboard.SetTargetProperty(myDoubleAnimation, new PropertyPath(Grid.OpacityProperty));
            myStoryBoard.Begin(this);

            if (to == 0)
                gridToAnimate.Visibility = Visibility.Hidden;
            else
                gridToAnimate.Visibility = Visibility.Visible;

        }

        private void FadeText(TextBlock gridToAnimate, double from, double to)
        {
            if (from == 1)
                gridToAnimate.Visibility = System.Windows.Visibility.Visible;
            else
                gridToAnimate.Visibility = System.Windows.Visibility.Hidden;

            DoubleAnimation myDoubleAnimation = new DoubleAnimation();
            myDoubleAnimation.From = from;
            myDoubleAnimation.To = to;
            myDoubleAnimation.Duration = new Duration(TimeSpan.FromSeconds(.2));
            myDoubleAnimation.AutoReverse = false;

            Storyboard myStoryBoard = new Storyboard();
            myStoryBoard.Children.Add(myDoubleAnimation);
            Storyboard.SetTargetName(myDoubleAnimation, gridToAnimate.Name);
            Storyboard.SetTargetProperty(myDoubleAnimation, new PropertyPath(Grid.OpacityProperty));
            myStoryBoard.Begin(this);

            if (to == 0)
                gridToAnimate.Visibility = Visibility.Hidden;
            else
                gridToAnimate.Visibility = Visibility.Visible;

        }


        private void ReSizeGroup(GroupBox groupToAnimate, double from, double to)
        {
            DoubleAnimation myDoubleAnimation = new DoubleAnimation();
            myDoubleAnimation.From = from;
            myDoubleAnimation.To = to;
            myDoubleAnimation.Duration = new Duration(TimeSpan.FromSeconds(.2));
            myDoubleAnimation.AutoReverse = false;

            Storyboard myStoryBoard = new Storyboard();
            myStoryBoard.Children.Add(myDoubleAnimation);
            Storyboard.SetTargetName(myDoubleAnimation, groupToAnimate.Name);
            Storyboard.SetTargetProperty(myDoubleAnimation, new PropertyPath(GroupBox.HeightProperty));
            myStoryBoard.Begin(this);

            ReSizeOtherGroups(to);
        }

        private void ReSizeOtherGroups(double to)
        {
            ThicknessAnimation myThicknessAnimation = new ThicknessAnimation();
            myThicknessAnimation.From = steamsettingsGroupBox.Margin;
            Thickness newThickness = new Thickness(steamsettingsGroupBox.Margin.Left, GroupBoxAccount.Margin.Top + 5 + to,
                steamsettingsGroupBox.Margin.Right, steamsettingsGroupBox.Margin.Bottom);
            myThicknessAnimation.To = newThickness;
            myThicknessAnimation.Duration = new Duration(TimeSpan.FromSeconds(.2));
            myThicknessAnimation.AutoReverse = false;

            Storyboard myStoryBoard = new Storyboard();
            myStoryBoard.Children.Add(myThicknessAnimation);
            Storyboard.SetTargetName(myThicknessAnimation, steamsettingsGroupBox.Name);
            Storyboard.SetTargetProperty(myThicknessAnimation, new PropertyPath(GroupBox.MarginProperty));
            myStoryBoard.Begin(this);

            ReSizeSettings(to);
        }

        private void ReSizeSettings(double to)
        {
            ThicknessAnimation myThicknessAnimation = new ThicknessAnimation();
            myThicknessAnimation.From = appsettingsGroupBox.Margin;
            Thickness newThickness = new Thickness(appsettingsGroupBox.Margin.Left, steamsettingsGroupBox.Height + 130 + to,
                appsettingsGroupBox.Margin.Right, appsettingsGroupBox.Margin.Bottom);
            myThicknessAnimation.To = newThickness;
            myThicknessAnimation.Duration = new Duration(TimeSpan.FromSeconds(.2));
            myThicknessAnimation.AutoReverse = false;

            Storyboard myStoryBoard = new Storyboard();
            myStoryBoard.Children.Add(myThicknessAnimation);
            Storyboard.SetTargetName(myThicknessAnimation, appsettingsGroupBox.Name);
            Storyboard.SetTargetProperty(myThicknessAnimation, new PropertyPath(GroupBox.MarginProperty));
            myStoryBoard.Begin(this);
        }


        private void saveSelectionButtonGoogle_Click(object sender, RoutedEventArgs e)
        {

            ListViewItem lvitem = listviewGoogleSheets.SelectedItem as ListViewItem;
            if (lvitem == null)
                MessageBox.Show("You must select a spreadsheet to continue");
            else
            {
                //GDrive.setGDriveSS(lvitem.Tag as SpreadsheetEntry);
                appSettings.setGoogleURI(lvitem.Tag as SpreadsheetEntry);
                appSettings.setGoogleCred(TextBoxGoogleUsername.Text, TextBoxGooglePassword.Password);
                appSettings.setActiveMode("GoogleDocs");
                activeModetext.Text = "GoogleDocs";
                FadeGrid(gridListViewGoogle, 1.0, 0.0);
                FadeGrid(CompletedGrid, 0.0, 1.0);
                ReSizeGroup(GroupBoxAccount, GroupBoxAccount.Height, 200);
            }
        }

        private void previousGridGoogle_Click(object sender, RoutedEventArgs e)
        {
            listviewGoogleSheets.ItemsSource = "";
            FadeGrid(gridListViewGoogle, 1.0, 0);
            FadeGrid(gridGoogle, 0, 1.0);
            startOverGrids();

        }

        private void ButtonRestartGrid_Click(object sender, RoutedEventArgs e)
        {
            FadeGrid(CompletedGrid, 1.0, 0.0);
            FadeGrid(gridOneAccount, 0.0, 1.0);

            appSettings.clear();

            startOverGrids();
        }

        private void buttonSaveOneAccount_Click(object sender, RoutedEventArgs e)
        {
            ReSizeGroup(GroupBoxAccount, GroupBoxAccount.Height, 200);

            CompletedGrid.Margin = new Thickness(0, 10, -215, 0);

            FadeGrid(gridOneAccount, 1.0, 0);
            FadeCombo(ComboBoxIntegration, 1.0, 0);
            FadeText(TextBlockIntegration, 1.0, 0);
            FadeGrid(CompletedGrid, 0, 1.0);           

            appSettings.setExecuteOnStart((bool)checkbox_startup.IsChecked);
            appSettings.setOneAccountCred(TextBoxUsername.Text, TextBoxPassword.Password);
            appSettings.setActiveMode("OneAccount");
            activeModetext.Text = "Steam One Account";
        }


        private void browseForSteamButton_Click(object sender, RoutedEventArgs e)
        {
            // Create an instance of the open file dialog box.
            OpenFileDialog openFileDialog1 = new OpenFileDialog();

            // Set filter options and filter index.
            openFileDialog1.Filter = "Steam (.exe)|Steam.exe";
            openFileDialog1.FilterIndex = 1;
            openFileDialog1.InitialDirectory = TextBoxSteamPath.Text;
            openFileDialog1.Multiselect = false;

            // Call the ShowDialog method to show the dialog box.
            bool? userClickedOK = openFileDialog1.ShowDialog();

            // Process input if the user clicked OK.
            if (userClickedOK == true)
            {
                // Open the selected file to read.
               // System.IO.Stream fileStream = openFileDialog1.File.OpenRead();
                TextBoxSteamPath.Text = openFileDialog1.FileName;
            }
        }

        #region CustomWindowControls
        //Custom Window Controls


        private void minimizeControl_MouseEnter(object sender, MouseEventArgs e)
        {
            (sender as TextBlock).Foreground = new SolidColorBrush(Color.FromRgb(0, 0, 0));
        }

        private void minimizeControl_MouseLeave(object sender, MouseEventArgs e)
        {
            (sender as TextBlock).Foreground = new SolidColorBrush(Color.FromRgb(255, 255, 255));
        }

        private void minimizeControl_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            WindowState = WindowState.Minimized;
        }

        private void closeControl_MouseEnter(object sender, MouseEventArgs e)
        {
            (sender as TextBlock).Foreground = new SolidColorBrush(Color.FromRgb(0, 0, 0));
        }

        private void closeControl_MouseLeave(object sender, MouseEventArgs e)
        {
            (sender as TextBlock).Foreground = new SolidColorBrush(Color.FromRgb(255, 255, 255));
        }

        private void closeControl_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            this.Close();
        }

        private void Window_MouseDown(object sender, MouseButtonEventArgs e)
        {
            if (e.ChangedButton == MouseButton.Left && closeControl.IsMouseDirectlyOver!=true && minimizeControl.IsMouseDirectlyOver!=true)
                this.DragMove();
        }
        #endregion
        private void ButtonCancel_Click(object sender, RoutedEventArgs e)
        {

        }

        private void checkbox_startup_Checked(object sender, RoutedEventArgs e)
        {

        }



    }
}
